#!/bin/bash
PROJECT_NAME="$1"
PROJECT_IP="$2"
PROJECT_ROOT="/home/project/projects"
DOCKER_DATA_ROOT="/home/project/docker_data"
PROJECT_TLD="test"

echo " = = = "
echo " PROJECT_NAME= ${PROJECT_NAME}"
echo " PROJECT_ROOT= ${PROJECT_ROOT}"
echo " = = = "


function vm_config_load {
	echo 'setup config'
	if [ -f /vagrant/vm.cfg ]; then
		source /vagrant/vm.cfg

		for CFG_KEY in $(cat /vagrant/vm.cfg | cut -d= -f1); do
        	export $CFG_KEY
    	done
	fi

	if [ "x$VM_GIT_USER_NAME" == "x" ]; then
		echo -e "\033[1;33mWARNING: Variable VM_GIT_USER_NAME has no value in vm.cfg, git user.name  will NOT be configured\033[0m"
	fi
	if [ "x$VM_GIT_USER_EMAIL" == "x" ]; then
		echo -e "\033[1;33mWARNING: Variable VM_GIT_USER_EMAIL has no value in vm.cfg, git user.email will NOT be configured\033[0m"
	fi
}

function setup_ssh_keys {
	echo 'setup ssh/keys'
	if [ ! -f /vagrant/id_rsa ]; then
		echo -e "\033[1;33mWARNING: id_rsa file not provided a new pair will be generated\033[0m"
		ssh-keygen -q -t rsa -m PEM -b 4096 -N "" -f /home/project/.ssh/id_rsa -C "${VM_LDAP_LOGIN:-"project"}@${PROJECT_NAME}.${PROJECT_TLD}"
		cp /home/project/.ssh/id_rsa /vagrant/id_rsa

		echo -e "Your public key is: " >> /etc/motd
		cat /home/project/.ssh/id_rsa.pub >> /etc/motd
		echo -e "\n\n" >> /etc/motd
	else
		if ! ssh-keygen -y -f /vagrant/id_rsa -P "" &> /dev/null; then
			echo -e "\033[0;31mERROR: id_rsa file MUST not contain a passphrase!\033[0m"
			exit 1
		fi

		cp /vagrant/id_rsa /home/project/.ssh/
		chmod 600 /home/project/.ssh/id_rsa
		ssh-keygen -y -f /home/project/.ssh/id_rsa > /home/project/.ssh/id_rsa.pub
	fi

	cat /home/project/.ssh/id_rsa.pub >> /home/project/.ssh/authorized_keys
	chown -R project:project /home/project/.ssh/
}

function setup_git {
	if [ ! -z "$VM_GIT_USER_NAME" ]; then
		echo 'setup git user.name from vm.cfg'
		git config --system user.name "$VM_GIT_USER_NAME"
	fi

	if [ ! -z "$VM_GIT_USER_EMAIL" ]; then
        echo 'setup git user.email from vm.cfg'
		git config --system user.email "$VM_GIT_USER_EMAIL"
    fi

}

function add_git_aliases {
	echo 'adding git aliases co/br/ci/st/df/lg'
	git config --system alias.co checkout
	git config --system alias.br branch
	git config --system alias.ci commit
	git config --system alias.st status
	git config --system alias.df diff
	git config --system alias.lg 'log -p'
}

function save_config_variables {
    if [ -f /vagrant/vm.cfg ]; then
		echo 'saving vm.cfg into environment'
        cat /vagrant/vm.cfg >> /etc/environment
    fi
}


vm_config_load
setup_ssh_keys
setup_git
add_git_aliases
save_config_variables


sudo -u project mkdir -p $PROJECT_ROOT
sudo -u project mkdir -p $DOCKER_DATA_ROOT

# make directories not that dark on dark background
echo 'DIR 30;47' > /home/project/.dircolors
chown project:project /home/project/.dircolors

# configure git
/usr/bin/git config --system push.default current
/usr/bin/git config --system color.ui auto
/usr/bin/git config --system core.fileMode true
sudo -u project /usr/bin/git config --global core.fileMode true


cd $PROJECT_ROOT
echo 'git cloning... '
sudo -u project /usr/bin/git clone git@bitbucket.org:makeeathappen/oml-php-api.git > /dev/null 2>&1 || true
sudo -u project /usr/bin/git clone git@bitbucket.org:makeeathappen/oml-fe.git > /dev/null 2>&1 || true

echo -e "\n## Update local /etc/hosts ...\n"
echo "${PROJECT_IP}            ${PROJECT_NAME}.${PROJECT_TLD}" | sudo tee -a /etc/hosts


# networking.sh does the rest
