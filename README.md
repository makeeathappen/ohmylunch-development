## Software dependencies
1. __git__
1. __VirtualBox__ https://www.virtualbox.org/wiki/Downloads
1. __Vagrant__ https://www.vagrantup.com/downloads.html


## Getting VM up and running

1. __clone__ this repo to your local file system.
1. __cd__ into cloned directory
1. Copy your current ssh private key or create new private key into this directory and name it __id_rsa__ (this key will be used while automatically cloning project source from repositories).
  
    __If you want to copy existing ssh key use (define your custom path)__
      ```
        cp ~/.ssh/id_rsa /your/custom/directory/ohmylunch-development/id_rsa
      ```
      
        * NOTE: Only keys without passphrase are supported.
        * NOTE: Make sure that your public key (created/copied) public part (id_rsa.__pub__) is added to Bitbucket SSH Keys section.

1. Give VM your credentials to automatically configure git with your name and email by filling information in file named `vm.cfg`.
1. Add this line to `/etc/hosts` file of your local host:
    ```
    10.24.1.2 docker-ohmylunch.test
    ```
1. Run `vagrant up` in the directory.
1. Run this command in the terminal to mount files from VM to your local host (define you local host path, where you cloned this repository):
    ```
    sudo mount docker-ohmylunch.test:/home/project/projects /your/custom/directory/ohmylunch-development/projects
    ```
   
    * NOTE: This command will be needed to run every time you restart your computer or VM.
    * NOTE: You can also use mount.sh script in the repository to do this by running `./mount.sh` from the terminal.

1. Run `vagrant ssh` to get logged into VM with no password required. Make sure your ssh
  client uses same private key as you copied in step 3.


__NOTE:__ Physically files resides inside virtual machine, so they are removed
permanently if you destroy a virtual machine. DO NOT forget to __commit__
and __push__ all your changes before stopping VM with `vagrant destroy`
