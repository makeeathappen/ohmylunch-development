#!/bin/bash

set -e
set -x

mkdir -p ~/Desktop/OML/projects
sudo mount -t nfs -o vers=3 -o nolocks 10.24.1.2:/var/www ~/Desktop/OML/projects

